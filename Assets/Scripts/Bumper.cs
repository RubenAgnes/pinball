﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {
	public Light light;
	public GameObject gm;
	
	void OnCollisionEnter(Collision other){
		gm.GetComponent<GameManager> ().UpdateScore (150);
		StartCoroutine (LightFlash());
	}

	private IEnumerator LightFlash(){
		light.gameObject.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		light.gameObject.SetActive (false);
	}
}
