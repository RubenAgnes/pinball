﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
	public GameObject gm;

	void OnCollisionEnter(Collision other){
		this.gameObject.SetActive (false);
		gm.GetComponent<GameManager> ().TargetDown();
	}
}
