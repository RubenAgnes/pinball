﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public GameObject ball;
	public Button start_btn;
	public Text score_text;
	public Text mult_text;

	public Light b1;
	public Light b2;
	public Light b3;

	public GameObject gameover_panel;

	public GameObject t1;
	public GameObject t2;
	public GameObject t3;
	private int targets;

	private int score;
	private int balls_left;
	private int mult;

	void Awake(){
		score = 0;
		balls_left = 4;
		mult = 1;
		targets = 0;
		score_text.text = score.ToString ();
		mult_text.text = "x" + mult.ToString ();
	}

	public void startGame(){
		ball.SetActive (true);
		StartCoroutine (DisableButton ());
	}

	private IEnumerator DisableButton(){
		yield return new WaitForSeconds (0.3f);
		start_btn.enabled = false;
		StopAllCoroutines ();
	}

	public void DecrementLifes(){
		balls_left--;
		switch (balls_left) {
		case 0:
			gameover_panel.SetActive (true);
			gameover_panel.transform.FindChild ("Score").gameObject.GetComponent<Text> ().text = "SCORE: " + score;
			break;
		case 1: b3.color = Color.red; break;
		case 2: b2.color = Color.red; break;
		case 3: b1.color = Color.red; break;
		}
	}

	public void UpdateMult(){
		if (mult < 4) {
			mult++;
			mult_text.text = "x " + mult.ToString ();
		}
	}

	public void UpdateScore(int sc){
		score = score + (sc * mult);
		score_text.text = score.ToString ();
	}

	public void TargetDown(){
		targets++;
		UpdateScore (320);
		if (targets == 3) {
			targets = 0;
			UpdateMult ();
			StartCoroutine (TargetsUp ());
		}
	}

	private IEnumerator TargetsUp(){
		yield return new WaitForSeconds (3f);
		t1.SetActive (true);
		t2.SetActive (true);
		t3.SetActive (true);
		StopAllCoroutines ();
	}

	public void Exit(){
		Application.Quit();
	}

	public int ballsNumber(){
		return this.balls_left;
	}
}
