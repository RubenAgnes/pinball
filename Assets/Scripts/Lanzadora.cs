﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanzadora : MonoBehaviour {
	private Rigidbody rbody;

	public bool charged;
	private float time;
	private bool pressed;

	public GameObject p1;
	public GameObject p2;
	public GameObject p3;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody> ();
		charged = true;
		pressed = false;
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (charged) {
				charged = false;
				time = 0.0f;
				pressed = true;
			}
		}

		time = time + Time.deltaTime;

		if (pressed) {
			if (time > 0.6f) {
				p1.SetActive (true);
			}
			if (time > 1.2f) {
				p2.SetActive (true);
			}
			if (time > 2f) {
				p3.SetActive (true);
			}
		}


		if (Input.GetKeyUp (KeyCode.Space)) {
			if (time > 2f) {
				rbody.AddForce (transform.forward * 60000);
				StartCoroutine (Recharge ());
			} else {
				rbody.AddForce (transform.forward * (80000 * (time * 0.25f)));
				StartCoroutine (Recharge ());
			}
			time = 0.0f;
			pressed = false;
			p1.SetActive (false);
			p2.SetActive (false);
			p3.SetActive (false);
		}
	}

	private IEnumerator Recharge()
	{
		yield return new WaitForSeconds (1f);
		rbody.AddForce (transform.forward * -60000);
		charged = true;
		StopAllCoroutines ();
	}
}
