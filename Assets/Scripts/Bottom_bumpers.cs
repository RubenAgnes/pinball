﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bottom_bumpers: MonoBehaviour {
	public Light light;

	void OnCollisionEnter(Collision other){
		StartCoroutine (LightFlash());
	}

	private IEnumerator LightFlash(){
		light.gameObject.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		light.gameObject.SetActive (false);
	}
}
