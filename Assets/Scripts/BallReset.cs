﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallReset : MonoBehaviour {
	public GameObject gm;
	public GameObject door;

	void OnTriggerEnter(Collider other){
		if (other.tag == "ball") {
			if (gm.GetComponent<GameManager> ().ballsNumber () > 0) {
				door.SetActive (false);
				other.gameObject.transform.localPosition = new Vector3 (17.86f,1.9f,-10.7f);
				gm.GetComponent<GameManager> ().DecrementLifes ();
			}
		}
	}
}
