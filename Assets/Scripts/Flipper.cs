﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {
	private Rigidbody rbody;
	public bool left = false;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!left) {
			if (Input.GetKey (KeyCode.RightControl)) {
				rbody.AddTorque (0, 100000, 0);
			} /*else {
				rbody.AddTorque (0, -100000, 0);
			}*/
		} else {
			if (Input.GetKey (KeyCode.LeftControl)) {
				rbody.AddTorque (0, -100000, 0);
			} /*else {
				rbody.AddTorque (0, 100000, 0);
			}*/
		}
	}
}
